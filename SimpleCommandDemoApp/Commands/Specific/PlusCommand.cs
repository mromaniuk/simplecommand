﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SimpleCommandDemoApp
{
    class PlusCommand : ICommand
    {

        private CalculatorViewModel calculatorViewModel;

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            Console.WriteLine("sprawdzanie plusCommand");
            return true;
        }

        public void Execute(object parameter)
        {
            Console.WriteLine("wykonano plusCommand");
            calculatorViewModel.Add();
        }

        public PlusCommand(CalculatorViewModel vm)
        {
            Console.WriteLine("utworzono plusCommand");
            calculatorViewModel = vm;
        }
    }
}
