﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SimpleCommandDemoApp
{
    class CalculatorViewModel : ViewModelBase
    {
        private double firstValue;
        private double secondValue;
        private double output;

        public double FirstValue
        {
            get
            {
                return firstValue;
            }
            set
            {
                Console.WriteLine("zmieniam FirstValue");
                firstValue = value;
                OnPropertyChanged("FirstValue");
            }
        }
        public double SecondValue
        {
            get
            {
                return secondValue;
            }
            set
            {
                Console.WriteLine("zmieniam SecondValue");
                secondValue = value;
                OnPropertyChanged("SecondValue");
            }
        }
        public double Output
        {
            get
            {
                return output;
            }
            set
            {
                Console.WriteLine("zmieniam Output");
                output = value;
                OnPropertyChanged("Output");
            }
        }

        public PlusCommand plusCommand;
        public CalculatorViewModel()
        {
            plusCommand = new PlusCommand(this);
        }

        public void Add()
        {
            Output = firstValue + secondValue;
        }

        internal ICommand AddCommand
        {
            get
            {
                Console.WriteLine("test+");
                return plusCommand;
                //return new RelayCommand(Add);
            }
            set { }
        }
    }
}
